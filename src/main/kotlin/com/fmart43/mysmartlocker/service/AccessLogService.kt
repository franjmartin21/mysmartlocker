package com.fmart43.mysmartlocker.service

import com.fmart43.mysmartlocker.model.AccessLog
import com.fmart43.mysmartlocker.model.Door
import com.fmart43.mysmartlocker.model.Employee
import com.fmart43.mysmartlocker.model.GrantedAccessEnum
import com.fmart43.mysmartlocker.repository.AccessLogRepository
import org.springframework.stereotype.Service

@Service
class AccessLogService(val accessLogRepository: AccessLogRepository){

    fun getAccessLog(doorId: Int? = null, employeeId: Int? = null, grantedAccess: GrantedAccessEnum? = null): List<AccessLog> {
        var accessLogList = accessLogRepository.findAll()

        if(doorId != null) accessLogList = accessLogList.filter { it -> it.door.id == doorId}

        if(employeeId != null) accessLogList = accessLogList.filter { it -> it.employee.id == employeeId}

        if(grantedAccess != null) accessLogList = accessLogList.filter { it -> it.grantedAccess == grantedAccess }

        return accessLogList
    }
}