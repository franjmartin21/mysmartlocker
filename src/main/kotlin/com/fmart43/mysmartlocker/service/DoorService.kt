package com.fmart43.mysmartlocker.service

import com.fmart43.mysmartlocker.controller.command.*
import com.fmart43.mysmartlocker.exception.NotFoundException
import com.fmart43.mysmartlocker.exception.ValidationException
import com.fmart43.mysmartlocker.model.Door
import com.fmart43.mysmartlocker.repository.DoorRepository
import org.springframework.stereotype.Service

@Service
class DoorService(val doorRepository: DoorRepository){

    fun getListDoors(): List<Door>? {
        return doorRepository.findAll()
    }

    fun findDoorCommandById(id: Int): DoorCommand? {
        val door = doorRepository.findById(id)

        return door?.toDoorCommand() ?: throw NotFoundException.DOOR(id)
    }

    fun saveDoorCommand(doorCommand: DoorCommand): DoorCommand?{
        var doorReturned: Door? = null
        validateNewDoor(doorCommand)

        if(doorCommand.id == null){
            doorReturned = doorRepository.save(doorCommand.toDoor())
        } else{
            val door = doorRepository.findById(doorCommand.id!!)
            door?.let {
                it.copyFromCommand(doorCommand)
                doorReturned = doorRepository.save(door)
            }
        }
        return doorReturned?.toDoorCommand()
    }

    fun validateNewDoor(doorCommand: DoorCommand){
        val existingDoors = getListDoors()
        val doorNfc = existingDoors?.filter { it.id != doorCommand.id && it.nfcCode == doorCommand.nfcCode }

        if(doorNfc?.isNotEmpty()!!) throw ValidationException.NFC_DOOR_EXISTS
    }

    fun deleteDoorCommand(doorCommand: DoorCommand){
        doorRepository.delete(doorCommand.toDoor())
    }

    fun findDoorValues():Map<Int, String>?{
        val doorList = doorRepository.findAll()

        return doorList?.map { it -> (it.id?:0) to it.doorName }?.toMap()
    }
}