package com.fmart43.mysmartlocker.service

import com.fmart43.mysmartlocker.restcontroller.v1.dto.RegistrationSettingDTO
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class ImplementationSettingService {

    @Value("\${conf.nfcCode}")
    val nfcCode: String? = null

    @Value("\${conf.companyName}")
    val companyName: String? = null

    fun getSettings(): RegistrationSettingDTO{
        return RegistrationSettingDTO(companyName, nfcCode)
    }
}