package com.fmart43.mysmartlocker.service

import com.fmart43.mysmartlocker.controller.command.EmployeeCommand
import com.fmart43.mysmartlocker.controller.command.toEmployee
import com.fmart43.mysmartlocker.controller.command.toEmployeeCommand
import com.fmart43.mysmartlocker.exception.NotFoundException
import com.fmart43.mysmartlocker.model.Employee
import com.fmart43.mysmartlocker.model.Role
import com.fmart43.mysmartlocker.repository.EmployeeRepository
import com.fmart43.mysmartlocker.restcontroller.v1.dto.EmployeeDTO
import com.fmart43.mysmartlocker.restcontroller.v1.dto.toEmployee
import com.fmart43.mysmartlocker.restcontroller.v1.dto.toEmployeeDTO
import org.springframework.stereotype.Service

@Service
class EmployeeService(val employeeRepository: EmployeeRepository){

    fun getListEmployees(role: Role? = null): List<Employee>? {
        var employeeList = employeeRepository.findAll()

        if(role != null) employeeList = employeeList?.filter {it -> it.role == role}

        return employeeList
    }

    fun findEmployeeCommandById(id: Int): EmployeeCommand?{
        val employee = employeeRepository.findById(id)
        return employee?.toEmployeeCommand() ?: throw NotFoundException.EMPLOYEE(id)
    }

    fun saveEmployeeCommand(employeeCommand: EmployeeCommand): EmployeeCommand?{
        val employee = employeeCommand.toEmployee()
        val savedEmployee = employeeRepository.save(employee)
        return savedEmployee?.toEmployeeCommand()
    }

    fun saveEmployeeDTO(employeeDTO: EmployeeDTO): EmployeeDTO?{
        val employee = employeeDTO.toEmployee()
        val savedEmployee = employeeRepository.save(employee)
        return savedEmployee?.toEmployeeDTO()
    }

    fun findUsernameValues():Map<Int, String>?{
        val employeeList = employeeRepository.findAll()

        return employeeList?.map { it -> (it.id?:0) to it.username }?.toMap()
    }
}