package com.fmart43.mysmartlocker.service

import com.fmart43.mysmartlocker.exception.AuthenticationFailedServiceException
import com.fmart43.mysmartlocker.exception.NotFoundException
import com.fmart43.mysmartlocker.exception.UnauthorizedServiceException
import com.fmart43.mysmartlocker.model.AccessLog
import com.fmart43.mysmartlocker.model.GrantedAccessEnum
import com.fmart43.mysmartlocker.repository.AccessLogRepository
import com.fmart43.mysmartlocker.repository.DoorRepository
import com.fmart43.mysmartlocker.repository.EmployeeRepository
import com.fmart43.mysmartlocker.restcontroller.v1.dto.CheckEmployeeDoorResponseDTO
import com.fmart43.mysmartlocker.restcontroller.v1.dto.EmployeeDoorDTO
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class AuthorizationService(val employeeRepository: EmployeeRepository, val doorRepository: DoorRepository, val accessLogRepository: AccessLogRepository){

    @Value("\${conf.companyName}")
    val companyName: String? = null

    fun isEmployeeAuthorizedForDoor(employeeDoorDTO: EmployeeDoorDTO): CheckEmployeeDoorResponseDTO? {
        var employee = employeeRepository.findByUsername(employeeDoorDTO.employeeUsername)?: throw NotFoundException.EMPLOYEE(employeeDoorDTO.employeeUsername)
        var door = doorRepository.findByNfcCode(employeeDoorDTO.doorNfcCode)?: throw NotFoundException.DOOR(employeeDoorDTO.doorNfcCode)

        return if(employee.role.levelAccess >= door.minLevelRole){
            CheckEmployeeDoorResponseDTO(companyName.toString(), door.doorName, employee.username)
        } else{
            accessLogRepository.save(AccessLog(employee, door, GrantedAccessEnum.DENIED))
            null
        }
    }

    fun authorizeEmployeeForDoor(employeeDoorDTO: EmployeeDoorDTO) {
        var employee = employeeRepository.findByUsername(employeeDoorDTO.employeeUsername)?: throw NotFoundException.EMPLOYEE(employeeDoorDTO.employeeUsername)
        if(employee.password != employeeDoorDTO.employeePassword) throw AuthenticationFailedServiceException()

        var door = doorRepository.findByNfcCode(employeeDoorDTO.doorNfcCode)?: throw NotFoundException.DOOR(employeeDoorDTO.doorNfcCode)

        return if(employee.role.levelAccess >= door.minLevelRole){
            accessLogRepository.save(AccessLog(employee, door, GrantedAccessEnum.GRANTED))
        } else{
            accessLogRepository.save(AccessLog(employee, door, GrantedAccessEnum.DENIED))
            throw UnauthorizedServiceException()
        }
    }
}