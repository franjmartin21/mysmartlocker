package com.fmart43.mysmartlocker.repository

import com.fmart43.mysmartlocker.model.AccessLog
import org.springframework.data.repository.Repository

interface AccessLogRepository: Repository<AccessLog, Int> {

    fun findAll(): List<AccessLog>

    fun save(accessLog: AccessLog)

}