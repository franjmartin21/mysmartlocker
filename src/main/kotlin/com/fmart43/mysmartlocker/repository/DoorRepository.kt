package com.fmart43.mysmartlocker.repository

import com.fmart43.mysmartlocker.model.Door
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.Repository

interface DoorRepository: Repository<Door, Int> {

    fun findAll(): List<Door>?

    fun findById(Id: Int): Door?

    @Query(value = "SELECT d FROM Door d WHERE d.nfcCode=?1")
    fun findByNfcCode(nfcCode: String): Door?

    fun save(door: Door): Door?

    fun delete(door:Door)

}