package com.fmart43.mysmartlocker.repository

import com.fmart43.mysmartlocker.model.Employee
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.Repository

interface EmployeeRepository: Repository<Employee, Int> {

    fun findAll(): List<Employee>?

    fun findById(Id: Int): Employee?

    @Query(value = "SELECT e FROM Employee e WHERE e.username=?1")
    fun findByUsername(username: String): Employee?

    fun save(employee: Employee): Employee?
}