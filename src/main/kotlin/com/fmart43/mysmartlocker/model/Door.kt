package com.fmart43.mysmartlocker.model

import javax.persistence.Entity

@Entity
data class Door(
        var doorName: String = "",
        var nfcCode: String = "",
        var minLevelRole: Int = 0
): BaseEntity()