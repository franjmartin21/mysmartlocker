package com.fmart43.mysmartlocker.model

enum class GrantedAccessEnum{
    GRANTED,
    DENIED
}