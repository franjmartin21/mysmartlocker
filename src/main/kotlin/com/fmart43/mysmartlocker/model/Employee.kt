package com.fmart43.mysmartlocker.model

import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated

@Entity
data class Employee(
        var username: String = "",
        var password: String = "",
        var firstName: String = "",
        var lastName: String = "",
        var fingerprintEnabled: Boolean = false,
        @Enumerated(EnumType.STRING)var role: Role = Role.UNAUTHORIZED
        ): BaseEntity()