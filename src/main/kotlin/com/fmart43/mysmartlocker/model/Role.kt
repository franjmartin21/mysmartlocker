package com.fmart43.mysmartlocker.model

enum class Role(val levelAccess: Int, val displayName: String){

    UNAUTHORIZED(0, "Unauthorized"),
    BASIC(1, "Basic"),
    ADVANCED(2, "Advanced"),
    ADMINISTRATOR(3, "Administrator");

    companion object{
        fun findRolesWithAnyAuthorization(): Array<Role>{
            return Role.values().filter { it.levelAccess > 0 }.toTypedArray()
        }
    }
}