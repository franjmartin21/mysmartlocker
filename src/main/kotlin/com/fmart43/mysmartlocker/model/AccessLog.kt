package com.fmart43.mysmartlocker.model

import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.ManyToOne

@Entity
data class AccessLog(
        @ManyToOne val employee: Employee,
        @ManyToOne val door: Door,
        @Enumerated(EnumType.STRING) val grantedAccess: GrantedAccessEnum
): BaseEntity()
