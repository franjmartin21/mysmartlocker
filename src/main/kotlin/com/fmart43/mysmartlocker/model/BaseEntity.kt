package com.fmart43.mysmartlocker.model

import java.io.Serializable
import java.time.LocalDateTime
import javax.persistence.*

@MappedSuperclass
open class BaseEntity: Serializable{

    @Id
    @GeneratedValue
    var id: Int? = null

    val isNew: Boolean
        get() = this.id == null

    var createDate: LocalDateTime? = null

    var updateDate: LocalDateTime? = null

    @PrePersist
    fun onPrePersist() {
        createDate = LocalDateTime.now()
        updateDate = LocalDateTime.now()
    }

    @PreUpdate
    fun onPreUpdate() {
        updateDate = LocalDateTime.now()
    }
}