package com.fmart43.mysmartlocker.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class NotFoundException(override var message:String): Exception(message){

    companion object {
        fun EMPLOYEE(id: Int) = NotFoundException("Employee not found for id: $id")
        fun EMPLOYEE(username: String) = NotFoundException("Employee not found for username: $username")
        fun DOOR(id: Int) = NotFoundException("Door not found for id: $id")
        fun DOOR(nfcCode: String) = NotFoundException("Door not found for nfc code: $nfcCode")
    }

    constructor():this("")

}


