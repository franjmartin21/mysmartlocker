package com.fmart43.mysmartlocker.exception

class ValidationException(override var message:String): Exception(message){

    companion object {
        val NFC_DOOR_EXISTS = ValidationException("The NFC code is already used for another door")

        val JSON_VALIDATION_PROBLEM = ValidationException("There was a problem when validating the rest object")
    }
}
