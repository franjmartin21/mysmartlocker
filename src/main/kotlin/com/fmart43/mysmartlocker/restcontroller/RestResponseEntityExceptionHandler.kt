package com.fmart43.mysmartlocker.restcontroller

import com.fmart43.mysmartlocker.exception.*
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler


@ControllerAdvice
class RestResponseEntityExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(ValidationException::class)
    fun handleValidationException(exception: Exception, request: WebRequest): ResponseEntity<Any> {

        return ResponseEntity("Validation Exception", HttpHeaders(), HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(NotFoundException::class)
    fun handleNotFoundException(exception: Exception, request: WebRequest): ResponseEntity<Any> {

        return ResponseEntity("Not Found Exception", HttpHeaders(), HttpStatus.EXPECTATION_FAILED)
    }

    @ExceptionHandler(UnknownServiceException::class)
    fun handleServerException(exception: Exception, request: WebRequest): ResponseEntity<Any> {

        return ResponseEntity("Server Error", HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR)
    }

    @ExceptionHandler(UnauthorizedServiceException::class)
    fun handleUnauthorizedException(exception: Exception, request: WebRequest): ResponseEntity<Any> {

        return ResponseEntity("Unauthorized employee", HttpHeaders(), HttpStatus.UNAUTHORIZED)
    }

    @ExceptionHandler(AuthenticationFailedServiceException::class)
    fun handleAuthenticationFailedServiceException(exception: Exception, request: WebRequest): ResponseEntity<Any> {

        return ResponseEntity("Authentication failed", HttpHeaders(), HttpStatus.BAD_REQUEST)
    }



}
