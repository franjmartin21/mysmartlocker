package com.fmart43.mysmartlocker.restcontroller.v1.dto

class ResponseDTO(val code: Int, val message: String? = null)