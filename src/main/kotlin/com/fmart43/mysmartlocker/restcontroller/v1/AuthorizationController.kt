package com.fmart43.mysmartlocker.restcontroller.v1

import com.fmart43.mysmartlocker.exception.UnauthorizedServiceException
import com.fmart43.mysmartlocker.restcontroller.v1.dto.CheckEmployeeDoorResponseDTO
import com.fmart43.mysmartlocker.restcontroller.v1.dto.EmployeeDoorDTO
import com.fmart43.mysmartlocker.service.AuthorizationService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/v1/authorization")
class AuthorizationController(val authorizationService: AuthorizationService){
    val logger = LoggerFactory.getLogger(AuthorizationController::class.java)

    @PostMapping("/check")
    @ResponseStatus(HttpStatus.OK)
    fun checkAuthorization(@RequestBody employeeDoorDTO: EmployeeDoorDTO): CheckEmployeeDoorResponseDTO {
        return authorizationService.isEmployeeAuthorizedForDoor(employeeDoorDTO)?: throw UnauthorizedServiceException()
    }

    @PostMapping("/authorize")
    @ResponseStatus(HttpStatus.OK)
    fun authorize(@RequestBody employeeDoorDTO: EmployeeDoorDTO) {
        authorizationService.authorizeEmployeeForDoor(employeeDoorDTO)
    }
}