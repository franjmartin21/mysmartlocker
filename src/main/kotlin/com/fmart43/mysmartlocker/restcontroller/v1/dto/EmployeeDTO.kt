package com.fmart43.mysmartlocker.restcontroller.v1.dto

import com.fmart43.mysmartlocker.model.Employee
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size

data class EmployeeDTO(
        var id: Int? = null,

        @field:NotEmpty
        @field:Size(min = 3, max = 30)
        var username: String = "",

        @field:NotEmpty
        @field:Size(min = 3, max = 20)
        var password: String = "",

        @field:NotEmpty
        @field:Size(min = 3, max = 20)
        var firstName: String = "",

        @field:NotEmpty
        @field:Size(min = 3, max = 20)
        var lastName: String = "",

        var fingerprintEnabled: Boolean = false
)

fun EmployeeDTO.toEmployee(): Employee {
    val employee = Employee()
    employee.username = this.username
    employee.password = this.password
    employee.firstName = this.firstName
    employee.lastName = this.lastName
    employee.fingerprintEnabled = this.fingerprintEnabled
    return employee
}

fun Employee.toEmployeeDTO(): EmployeeDTO {
    val employeeDTO = EmployeeDTO(this.id)
    employeeDTO.username = this.username
    employeeDTO.password = this.password
    employeeDTO.firstName = this.firstName
    employeeDTO.lastName = this.lastName
    employeeDTO.fingerprintEnabled = this.fingerprintEnabled
    return employeeDTO
}
