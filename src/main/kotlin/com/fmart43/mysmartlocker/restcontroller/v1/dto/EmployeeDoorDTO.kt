package com.fmart43.mysmartlocker.restcontroller.v1.dto

data class EmployeeDoorDTO(
    var doorNfcCode: String = "",
    val employeeUsername: String = "",
    val employeePassword: String = ""
)