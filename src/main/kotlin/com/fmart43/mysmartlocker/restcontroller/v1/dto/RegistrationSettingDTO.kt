package com.fmart43.mysmartlocker.restcontroller.v1.dto

data class RegistrationSettingDTO(val nameCompany: String?, val nfcCode: String?)