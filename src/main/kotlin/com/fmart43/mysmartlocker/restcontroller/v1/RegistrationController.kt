package com.fmart43.mysmartlocker.restcontroller.v1

import com.fmart43.mysmartlocker.exception.UnknownServiceException
import com.fmart43.mysmartlocker.exception.ValidationException
import com.fmart43.mysmartlocker.restcontroller.v1.dto.EmployeeDTO
import com.fmart43.mysmartlocker.restcontroller.v1.dto.RegistrationSettingDTO
import com.fmart43.mysmartlocker.service.EmployeeService
import com.fmart43.mysmartlocker.service.ImplementationSettingService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/v1/registration")
class RegistrationController(val implementationSettingService: ImplementationSettingService, val employeeService: EmployeeService){
    val logger = LoggerFactory.getLogger(RegistrationController::class.java)

    @GetMapping("/start")
    @ResponseStatus(HttpStatus.OK)
    fun startRegistration(): RegistrationSettingDTO {
        logger.debug("Called startRegistration")
        return implementationSettingService.getSettings()
    }

    @PostMapping("/complete")
    @ResponseStatus(HttpStatus.CREATED)
    fun completeRegistration(@Valid @RequestBody employeeDTO: EmployeeDTO, bindingResult: BindingResult) {
        logger.debug("Called completeRegistration  with data $employeeDTO")
        if (bindingResult.hasErrors()) {
            bindingResult.allErrors.forEach { logger.debug("VALIDATION MESSAGE: $it") }
            throw ValidationException.JSON_VALIDATION_PROBLEM
        }
        employeeService.saveEmployeeDTO(employeeDTO)?:throw UnknownServiceException()
        //return if(employeeDTO != null) ResponseDTO(HttpStatus.CREATED.value()) else ResponseDTO(HttpStatus.INTERNAL_SERVER_ERROR.value(), "")
    }

}