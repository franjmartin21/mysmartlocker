package com.fmart43.mysmartlocker.restcontroller.v1.dto

data class CheckEmployeeDoorResponseDTO(
        val nameCompany: String,
        val doorName: String,
        val userName: String
)
