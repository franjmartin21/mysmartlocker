package com.fmart43.mysmartlocker.bootstrap

import com.fmart43.mysmartlocker.model.*
import com.fmart43.mysmartlocker.repository.AccessLogRepository
import com.fmart43.mysmartlocker.repository.DoorRepository
import com.fmart43.mysmartlocker.repository.EmployeeRepository
import org.springframework.context.ApplicationListener
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.stereotype.Component

@Component
class BootstrapInitialData(
        val employeeRepository: EmployeeRepository,
        val doorRepository: DoorRepository,
        val accessLogRepository: AccessLogRepository
): ApplicationListener<ContextRefreshedEvent> {

    override fun onApplicationEvent(event: ContextRefreshedEvent) {
        initData()
    }

    private fun initData(){
        val door = Door("Kitchen door", "2", 1)
        val door2 = Door("Secret Protected Room", "3", 2)
        doorRepository.save(door)
        doorRepository.save(door2)

        val employeeUnauth = Employee("uauthUser", "adsf", "Unauth", "User", true, Role.UNAUTHORIZED)
        val employeeBasic = Employee("basicUser", "adsf", "Basic", "User", true, Role.BASIC)
        val employeeAdvanced = Employee("advancedUser", "adsf", "Advanced", "User", false, Role.ADVANCED)
        val employeeAdministrator= Employee("administratorUser", "adsf", "Admin", "User", true, Role.ADMINISTRATOR)

        employeeRepository.save(employeeUnauth)
        employeeRepository.save(employeeBasic)
        employeeRepository.save(employeeAdvanced)
        employeeRepository.save(employeeAdministrator)

        val accessLog0 = AccessLog(employeeUnauth, door, GrantedAccessEnum.DENIED)
        val accessLog1 = AccessLog(employeeBasic, door, GrantedAccessEnum.GRANTED)
        val accessLog2 = AccessLog(employeeBasic, door2, GrantedAccessEnum.DENIED)
        val accessLog3 = AccessLog(employeeAdvanced, door2, GrantedAccessEnum.GRANTED)
        val accessLog4 = AccessLog(employeeAdministrator, door2, GrantedAccessEnum.GRANTED)

        accessLogRepository.save(accessLog0)
        accessLogRepository.save(accessLog1)
        accessLogRepository.save(accessLog2)
        accessLogRepository.save(accessLog3)
        accessLogRepository.save(accessLog4)

    }
}