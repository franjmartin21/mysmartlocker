package com.fmart43.mysmartlocker.controller

import com.fmart43.mysmartlocker.exception.NotFoundException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.servlet.ModelAndView

open class BaseController{

    val ERROR_404: String = "error/404error"
    val ERROR_400: String = "error/400error"

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException::class)
    fun handleNotFound(exception: Exception): ModelAndView {
        val modelAndView = ModelAndView()

        modelAndView.viewName = ERROR_404
        modelAndView.addObject("exception", exception)

        return modelAndView
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(NumberFormatException::class)
    fun handleNumberFormat(exception: Exception): ModelAndView {
        val modelAndView = ModelAndView()

        modelAndView.viewName = ERROR_400
        modelAndView.addObject("exception", exception)

        return modelAndView
    }
}