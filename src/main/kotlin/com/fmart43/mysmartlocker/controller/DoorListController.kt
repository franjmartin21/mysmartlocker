package com.fmart43.mysmartlocker.controller

import com.fmart43.mysmartlocker.model.Door
import com.fmart43.mysmartlocker.service.DoorService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/doorlist")
class DoorListController(val doorService: DoorService){

    val VIEW_DOOR_LIST = "door/doorList"

    val DOOR_LIST_MODEL_NAME = "doorList"

    @RequestMapping("", "/", "/index")
    fun initListDoor(model: MutableMap<String, Any>): String {
        //val owner = Owner()
        setModelByCriteria(model)
        return VIEW_DOOR_LIST
    }


    private fun setModelByCriteria(model: MutableMap<String, Any>){
        val doorList: List<Door>

        doorList = doorService.getListDoors()?: listOf()
        model[DOOR_LIST_MODEL_NAME] = doorList
    }
}