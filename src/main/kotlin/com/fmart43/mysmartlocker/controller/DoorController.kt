package com.fmart43.mysmartlocker.controller

import com.fmart43.mysmartlocker.controller.command.DoorCommand
import com.fmart43.mysmartlocker.exception.ValidationException
import com.fmart43.mysmartlocker.model.Role
import com.fmart43.mysmartlocker.service.DoorService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.validation.BindingResult
import javax.validation.Valid

@Controller
@RequestMapping("/door")
class DoorController(val doorService: DoorService): BaseController(){

    val logger = LoggerFactory.getLogger(DoorController::class.java)

    val VIEW_DOOR = "door/doorEdit"
    val DOOR_COMMAND = "doorCommand"
    val ROLE_LEVEL_MODEL = "roleLevelModel"

    val ERROR_COMMAND = "errorCommand"

    @GetMapping("/{id}/edit")
    fun editDoor(@PathVariable id: Int, model: MutableMap<String, Any>): String {
        val doorCommand = doorService.findDoorCommandById(id)
        informModelValues(model, doorCommand)
        return VIEW_DOOR
    }

    @GetMapping("/new")
    fun newDoor(model: MutableMap<String, Any>): String {
        informModelValues(model)
        return VIEW_DOOR
    }

    private fun informModelValues(model: MutableMap<String, Any>, doorCommand: DoorCommand? = null){
        model[DOOR_COMMAND] = doorCommand?: DoorCommand()
        model[ROLE_LEVEL_MODEL] = Role.findRolesWithAnyAuthorization()
    }

    @PostMapping("/{id}/save")
    fun saveOrUpdateDoor(@Valid @ModelAttribute doorCommand: DoorCommand, bindingResult: BindingResult, model: MutableMap<String, Any>): String {
        if (bindingResult.hasErrors()) {
            bindingResult.allErrors.forEach {logger.debug("VALIDATION MESSAGE: $it")}
            informModelValues(model, doorCommand)
            return VIEW_DOOR
        }
        try {
            doorService.saveDoorCommand(doorCommand)
        }catch (e: ValidationException){
            model[ERROR_COMMAND] = e.message
            return VIEW_DOOR
        }

        return "redirect:/doorlist/index"
    }

    @GetMapping("/{id}/delete")
    fun deleteDoor(@ModelAttribute doorCommand: DoorCommand): String {
        doorService.deleteDoorCommand(doorCommand)

        return "redirect:/doorlist/index"
    }
}