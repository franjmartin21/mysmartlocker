package com.fmart43.mysmartlocker.controller

import com.fmart43.mysmartlocker.controller.command.EmployeeCommand
import com.fmart43.mysmartlocker.service.EmployeeService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Controller
@RequestMapping("/employee/{id}")
class EmployeeController(val employeeService: EmployeeService): BaseController(){
    val logger = LoggerFactory.getLogger(DoorController::class.java)

    val VIEW_EMPLOYEE = "employee/employeeEdit"
    val EMPLOYEE_COMMAND = "employeeCommand"

    @RequestMapping("/edit")
    fun initEmployee(@PathVariable id: Int, model: MutableMap<String, Any>): String {
        val employeeCommand = employeeService.findEmployeeCommandById(id)
        if(employeeCommand != null) model[EMPLOYEE_COMMAND] = employeeCommand

        return VIEW_EMPLOYEE
    }

    @PostMapping("/save")
    fun saveOrUpdate(@Valid @ModelAttribute employeeCommand: EmployeeCommand, bindingResult: BindingResult): String {
        if (bindingResult.hasErrors()) {

            bindingResult.allErrors.forEach { logger.debug("VALIDATION MESSAGE: $it") }

            return VIEW_EMPLOYEE
        }

        employeeService.saveEmployeeCommand(employeeCommand)

        return "redirect:/employeelist/index"
    }
}