package com.fmart43.mysmartlocker.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
class IndexController{

    @RequestMapping("/", "/index")
    fun initIndex(model: MutableMap<String, Any>): String {
        return "redirect:/employeelist/index"
    }
}