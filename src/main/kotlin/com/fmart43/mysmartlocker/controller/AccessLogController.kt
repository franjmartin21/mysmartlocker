package com.fmart43.mysmartlocker.controller

import com.fmart43.mysmartlocker.controller.command.AccessLogFiltersCommand
import com.fmart43.mysmartlocker.model.AccessLog
import com.fmart43.mysmartlocker.service.AccessLogService
import com.fmart43.mysmartlocker.service.DoorService
import com.fmart43.mysmartlocker.service.EmployeeService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/accesslog")
class AccessLogController(val accessLogService: AccessLogService, val employeeService: EmployeeService, val doorService: DoorService){

    val VIEW_ACCESS_LOG_LIST = "accessLog/accessLog"

    val ACCESS_LOG_LIST_MODEL_NAME = "accessLogList"

    val ACCESS_LOG_FILTERS = "accessLogFilters"

    val ACCESS_LOG_DOOR_VALUES = "accessLogDoorValues"

    val ACCESS_LOG_USERNAME_VALUES = "accessLogUsernameValues"

    @RequestMapping("", "/", "/index")
    fun initAccessLog(model: MutableMap<String, Any>): String {
        setModelByCriteria(model)
        return VIEW_ACCESS_LOG_LIST
    }

    @RequestMapping("/applyfilters")
    fun applyFiltersAccessLog(@ModelAttribute command: AccessLogFiltersCommand, model: MutableMap<String, Any>): String {
        setModelByCriteria(model, command)
        return VIEW_ACCESS_LOG_LIST
    }

    private fun setModelByCriteria(model: MutableMap<String, Any>, accessLogFilters: AccessLogFiltersCommand? = null){
        val accessLogList: List<AccessLog> = accessLogService.getAccessLog(doorId = accessLogFilters?.doorId, employeeId = accessLogFilters?.employeeId, grantedAccess = accessLogFilters?.grantedAccess)
        model[ACCESS_LOG_LIST_MODEL_NAME] = accessLogList
        model[ACCESS_LOG_FILTERS] = AccessLogFiltersCommand()
        model[ACCESS_LOG_USERNAME_VALUES] = employeeService.findUsernameValues()?: mapOf<Int, String>()
        model[ACCESS_LOG_DOOR_VALUES] = doorService.findDoorValues()?: mapOf<Int, String>()
    }
}