package com.fmart43.mysmartlocker.controller.command

import com.fmart43.mysmartlocker.model.GrantedAccessEnum


data class AccessLogFiltersCommand(
       val employeeId: Int? = null,
       val doorId: Int? = null,
       val grantedAccess: GrantedAccessEnum? = null
)
