package com.fmart43.mysmartlocker.controller.command

import com.fmart43.mysmartlocker.model.Employee
import com.fmart43.mysmartlocker.model.Role
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size

data class EmployeeCommand(
        var id: Int?,

        @field:NotEmpty
        @field:Size(min = 3, max = 30)
        var username: String = "",

        @field:NotEmpty
        @field:Size(min = 3, max = 20)
        var password: String = "",

        @field:NotEmpty
        @field:Size(min = 3, max = 20)
        var firstName: String = "",

        @field:NotEmpty
        @field:Size(min = 3, max = 20)
        var lastName: String = "",

        var fingerprintEnabled: Boolean = false,

        var role: Role = Role.UNAUTHORIZED
)

fun EmployeeCommand.toEmployee(): Employee {
    val employee = Employee()
    employee.id = this.id
    employee.username = this.username
    employee.password = this.password
    employee.firstName = this.firstName
    employee.lastName = this.lastName
    employee.fingerprintEnabled = this.fingerprintEnabled
    employee.role = this.role
    return employee
}

fun Employee.toEmployeeCommand(): EmployeeCommand{
    val employeeCommand = EmployeeCommand(this.id)
    employeeCommand.username = this.username
    employeeCommand.password = this.password
    employeeCommand.firstName = this.firstName
    employeeCommand.lastName = this.lastName
    employeeCommand.fingerprintEnabled = this.fingerprintEnabled
    employeeCommand.role = this.role
    return employeeCommand
}
