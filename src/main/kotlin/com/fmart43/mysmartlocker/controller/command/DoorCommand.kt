package com.fmart43.mysmartlocker.controller.command

import com.fmart43.mysmartlocker.model.Door
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size

data class DoorCommand(
        var id: Int? = null,

        @field:NotEmpty
        @field:Size(min = 3, max = 20)
        var doorName: String = "",

        @field:NotEmpty
        @field:Size(min = 1, max = 5)
        var nfcCode: String = "",

        @field:Min(1)
        @field:Max(3)
        var minLevelRole: Int = 0
)

fun DoorCommand.toDoor(): Door {
    val door = Door()
    door.id = this.id
    door.doorName = this.doorName
    door.nfcCode = this.nfcCode
    door.minLevelRole = this.minLevelRole
    return door
}

fun Door.toDoorCommand(): DoorCommand{
    val doorCommand = DoorCommand(this.id)
    doorCommand.doorName = this.doorName
    doorCommand.nfcCode = this.nfcCode
    doorCommand.minLevelRole = this.minLevelRole
    return doorCommand
}

fun Door.copyFromCommand(doorCommand:DoorCommand){
    this.doorName = doorCommand.doorName
    this.nfcCode= doorCommand.nfcCode
    this.minLevelRole = doorCommand.minLevelRole
}
