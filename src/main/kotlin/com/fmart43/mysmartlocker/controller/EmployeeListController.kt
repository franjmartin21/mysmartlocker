package com.fmart43.mysmartlocker.controller

import com.fmart43.mysmartlocker.model.Employee
import com.fmart43.mysmartlocker.model.Role
import com.fmart43.mysmartlocker.service.EmployeeService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/employeelist")
class EmployeeListController(val employeeService: EmployeeService){

    val VIEW_EMPLOYEE_LIST = "employee/employeeList"

    val EMPLOYEE_LIST_MODEL_NAME = "employeeList"

    @RequestMapping("", "/", "/index")
    fun initListEmployee(model: MutableMap<String, Any>): String {
        //val owner = Owner()
        setModelByCriteria(model)
        return VIEW_EMPLOYEE_LIST
    }

    @RequestMapping("/role/{role}")
    fun listEmployeeByRole(@PathVariable role: Role, model: MutableMap<String, Any>): String{
        setModelByCriteria(model, role)
        return VIEW_EMPLOYEE_LIST
    }


    private fun setModelByCriteria(model: MutableMap<String, Any>, role: Role? = null){
        val employeeList: List<Employee>

        employeeList = employeeService.getListEmployees(role)?: listOf<Employee>()

        model[EMPLOYEE_LIST_MODEL_NAME] = employeeList
    }
}