package com.fmart43.mysmartlocker

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MysmartlockerApplication

fun main(args: Array<String>) {
    runApplication<MysmartlockerApplication>(*args)
}
