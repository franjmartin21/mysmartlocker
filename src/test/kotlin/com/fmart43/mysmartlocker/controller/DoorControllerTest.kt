package com.fmart43.mysmartlocker.controller

import com.fmart43.mysmartlocker.BaseDataMockTest
import com.fmart43.mysmartlocker.controller.command.DoorCommand
import com.fmart43.mysmartlocker.service.DoorService
import org.hamcrest.Matchers
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.setup.MockMvcBuilders

internal class DoorControllerTest: BaseDataMockTest(){

    val doorService = Mockito.mock(DoorService::class.java)

    var doorController: DoorController? = null

    @BeforeEach
    fun setup(){
        doorController = DoorController(doorService)
    }

    @Test
    fun editDoor_success(){
        val mockMvc: MockMvc = MockMvcBuilders.standaloneSetup(doorController).build()

        Mockito.`when`(doorService.findDoorCommandById(1)).thenReturn(doorCommand1)

        mockMvc.perform(get("/door/1/edit"))
                .andExpect(status().isOk)
                .andExpect(view().name(doorController!!.VIEW_DOOR))
                .andExpect(model().attributeExists(doorController!!.DOOR_COMMAND))
                .andExpect(model().attribute(doorController!!.DOOR_COMMAND, Matchers.equalTo(doorCommand1)))

        Mockito.verify(doorService, Mockito.times(1)).findDoorCommandById(1)
    }

    @Test
    fun createDoor_success(){
        val mockMvc: MockMvc = MockMvcBuilders.standaloneSetup(doorController).build()

        val newDoorCommand = DoorCommand()

        mockMvc.perform(get("/door/new"))
                .andExpect(status().isOk)
                .andExpect(view().name(doorController!!.VIEW_DOOR))
                .andExpect(model().attributeExists(doorController!!.DOOR_COMMAND))
                .andExpect(model().attribute(doorController!!.DOOR_COMMAND, Matchers.equalTo(newDoorCommand)))

    }

    @Test
    fun saveOrUpdate_success() {
        val mockMvc: MockMvc = MockMvcBuilders.standaloneSetup(doorController).build()

        var doorCommand = DoorCommand(3, "door3", "NF1", 2)

        Mockito.`when`(doorService.saveDoorCommand(doorCommand)).thenReturn(doorCommand)

        mockMvc.perform(post("/door/3/save")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("doorName", "door3")
                .param("nfcCode", "NF1")
                .param("minLevelRole", "2"))
                .andExpect(status().is3xxRedirection)

        Mockito.verify(doorService, Mockito.times(1)).saveDoorCommand(doorCommand)
    }

    @Test
    fun saveOrUpdate_validationError() {
        val mockMvc: MockMvc = MockMvcBuilders.standaloneSetup(doorController).build()

        var doorCommand = DoorCommand(3, doorName = "door3")

        mockMvc.perform(post("/door/3/save")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("doorName", "door3"))
                .andExpect(status().is2xxSuccessful)
                .andExpect(view().name(doorController!!.VIEW_DOOR))

        Mockito.verify(doorService, Mockito.times(0)).saveDoorCommand(doorCommand)
    }

    @Test
    fun deleteDoor_success() {
        val mockMvc: MockMvc = MockMvcBuilders.standaloneSetup(doorController).build()

        var doorCommand = DoorCommand(3, doorName = "door3")

        mockMvc.perform(get("/door/3/delete")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("doorName", "door3"))
                .andExpect(status().is3xxRedirection)

        Mockito.verify(doorService, Mockito.times(1)).deleteDoorCommand(doorCommand)
    }
}