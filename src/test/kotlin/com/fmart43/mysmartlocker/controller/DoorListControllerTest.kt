package com.fmart43.mysmartlocker.controller

import com.fmart43.mysmartlocker.BaseDataMockTest
import com.fmart43.mysmartlocker.model.Door
import com.fmart43.mysmartlocker.model.Role
import com.fmart43.mysmartlocker.service.DoorService
import org.hamcrest.Matchers
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.setup.MockMvcBuilders

internal class DoorListControllerTest: BaseDataMockTest(){

    val doorService = Mockito.mock(DoorService::class.java)

    var doorListController: DoorListController? = null

    @BeforeEach
    fun setup(){
        doorListController = DoorListController(doorService)
    }

    @Test
    fun listEmployee_success(){
        val mockMvc: MockMvc = MockMvcBuilders.standaloneSetup(doorListController).build()
        val doorListReturned: List<Door> = listOf(door1)

        Mockito.`when`(doorService.getListDoors()).thenReturn(doorListReturned)

        mockMvc.perform(get("/doorlist/index"))
                .andExpect(status().isOk)
                .andExpect(view().name(doorListController!!.VIEW_DOOR_LIST))
                .andExpect(model().attributeExists(doorListController!!.DOOR_LIST_MODEL_NAME))
                .andExpect(model().attribute(doorListController!!.DOOR_LIST_MODEL_NAME, Matchers.hasItem(door1)))

        Mockito.verify(doorService, Mockito.times(1)).getListDoors()
    }

}