package com.fmart43.mysmartlocker.controller

import com.fmart43.mysmartlocker.BaseDataMockTest
import com.fmart43.mysmartlocker.model.AccessLog
import com.fmart43.mysmartlocker.service.AccessLogService
import com.fmart43.mysmartlocker.service.DoorService
import com.fmart43.mysmartlocker.service.EmployeeService
import org.hamcrest.Matchers
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.setup.MockMvcBuilders

internal class AccessLogListControllerTest: BaseDataMockTest(){

    val accessLogService = Mockito.mock(AccessLogService::class.java)

    val employeeService = Mockito.mock(EmployeeService::class.java)

    val doorService = Mockito.mock(DoorService::class.java)

    var accessLogController: AccessLogController? = null

    @BeforeEach
    fun setup(){
        accessLogController = AccessLogController(accessLogService, employeeService, doorService)
    }

    @Test
    fun listAccessLog_success(){
        val mockMvc: MockMvc = MockMvcBuilders.standaloneSetup(accessLogController).build()
        val accessListReturned: List<AccessLog> = listOf(accessLog1, accessLog2)

        Mockito.`when`(accessLogService.getAccessLog()).thenReturn(accessListReturned)

        mockMvc.perform(get("/accesslog/index"))
                .andExpect(status().isOk)
                .andExpect(view().name(accessLogController!!.VIEW_ACCESS_LOG_LIST))
                .andExpect(model().attributeExists(accessLogController!!.ACCESS_LOG_LIST_MODEL_NAME))
                .andExpect(model().attributeExists(accessLogController!!.ACCESS_LOG_FILTERS))
                .andExpect(model().attributeExists(accessLogController!!.ACCESS_LOG_USERNAME_VALUES))
                .andExpect(model().attributeExists(accessLogController!!.ACCESS_LOG_DOOR_VALUES))
                .andExpect(model().attribute(accessLogController!!.ACCESS_LOG_LIST_MODEL_NAME, Matchers.hasItems(accessLog1, accessLog2)))

        Mockito.verify(accessLogService, Mockito.times(1)).getAccessLog()
    }

}