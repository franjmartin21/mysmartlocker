package com.fmart43.mysmartlocker.controller

import com.fmart43.mysmartlocker.BaseDataMockTest
import com.fmart43.mysmartlocker.model.Employee
import com.fmart43.mysmartlocker.model.Role
import com.fmart43.mysmartlocker.service.EmployeeService
import org.hamcrest.Matchers
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.view
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.model

internal class EmployeeListControllerTest: BaseDataMockTest(){

    val employeeService = Mockito.mock(EmployeeService::class.java)

    var employeeListController: EmployeeListController? = null

    @BeforeEach
    fun setup(){
        employeeListController = EmployeeListController(employeeService)
    }

    @Test
    fun listEmployee_success(){
        val mockMvc: MockMvc = MockMvcBuilders.standaloneSetup(employeeListController).build()
        val employeeListReturned: List<Employee> = listOf(employee1, employee2)

        Mockito.`when`(employeeService.getListEmployees()).thenReturn(employeeListReturned)

        mockMvc.perform(get("/employeelist/index"))
                .andExpect(status().isOk)
                .andExpect(view().name(employeeListController!!.VIEW_EMPLOYEE_LIST))
                .andExpect(model().attribute(employeeListController!!.EMPLOYEE_LIST_MODEL_NAME, Matchers.hasItem(employee1)))

        Mockito.verify(employeeService, Mockito.times(1)).getListEmployees()
    }

    @Test
    fun listEmployeeByRole_success(){
        val mockMvc: MockMvc = MockMvcBuilders.standaloneSetup(employeeListController).build()
        val employeeListReturned: List<Employee> = listOf(employee1)

        Mockito.`when`(employeeService.getListEmployees(Role.BASIC)).thenReturn(employeeListReturned)

        mockMvc.perform(get("/employeelist/role/BASIC"))
                .andExpect(status().isOk)
                .andExpect(view().name(employeeListController!!.VIEW_EMPLOYEE_LIST))
                .andExpect(model().attribute(employeeListController!!.EMPLOYEE_LIST_MODEL_NAME, Matchers.hasItem(employee1)))

        Mockito.verify(employeeService, Mockito.times(1)).getListEmployees(Role.BASIC)
    }
}