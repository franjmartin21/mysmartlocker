package com.fmart43.mysmartlocker.controller.command

import com.fmart43.mysmartlocker.BaseDataMockTest
import com.fmart43.mysmartlocker.model.Employee
import com.fmart43.mysmartlocker.model.Role
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class EmployeeCommandTest: BaseDataMockTest(){

    @Test
    fun convertEmployeeCommandToEntity_success(){
        val commandId = 1
        val commandUsername = "abc"
        val commandPassword = "password"
        val commandFirstName = "firstName"
        val commandLastName = "lastName"
        val commandFingerprintEnabled = true
        val commandRole = Role.ADMINISTRATOR
        val employeeCommand: EmployeeCommand = EmployeeCommand(commandId, commandUsername, commandPassword, commandFirstName, commandLastName, commandFingerprintEnabled, commandRole)
        val employee: Employee = employeeCommand.toEmployee()

        assertEquals(commandId, employee.id)
        assertEquals(commandUsername, employee.username)
        assertEquals(commandPassword, employee.password)
        assertEquals(commandFirstName, employee.firstName)
        assertEquals(commandLastName, employee.lastName)
        assertEquals(commandFingerprintEnabled, employee.fingerprintEnabled)
        assertEquals(commandRole, employee.role)
    }

    @Test
    fun convertEmployeeEntityToCommand_success(){
        employee1.id = 1
        val employeeCommand = this.employee1.toEmployeeCommand()
        assertEquals(employee1.id, employeeCommand.id)
        assertEquals(employee1.username, employeeCommand.username)
        assertEquals(employee1.password, employeeCommand.password)
        assertEquals(employee1.firstName, employeeCommand.firstName)
        assertEquals(employee1.lastName, employeeCommand.lastName)
        assertEquals(employee1.fingerprintEnabled, employeeCommand.fingerprintEnabled)
        assertEquals(employee1.role, employeeCommand.role)
    }
}