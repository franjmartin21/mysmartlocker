package com.fmart43.mysmartlocker.controller.command

import com.fmart43.mysmartlocker.BaseDataMockTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class DoorCommandTest: BaseDataMockTest(){

    @Test
    fun convertEmployeeCommandToEntity_success(){
        val commandId = 1
        val commandName = "Door-1"
        val commandNfc = "DOOR-1"
        val commandMinLevelRole = 3

        val doorCommand = DoorCommand(commandId, commandName, commandNfc, commandMinLevelRole)
        val door = doorCommand.toDoor()

        assertEquals(commandId, door.id)
        assertEquals(commandName, door.doorName)
        assertEquals(commandNfc, door.nfcCode)
        assertEquals(commandMinLevelRole, door.minLevelRole)
    }

    @Test
    fun convertEmployeeEntityToCommand_success(){
        door1.id = 1
        val doorCommand = this.door1.toDoorCommand()
        assertEquals(door1.id, doorCommand.id)
        assertEquals(door1.doorName, doorCommand.doorName)
        assertEquals(door1.nfcCode, doorCommand.nfcCode)
        assertEquals(door1.minLevelRole, doorCommand.minLevelRole)
    }

    @Test
    fun copyFromCommand_success(){
        door1.id = 1
        door1.copyFromCommand(doorCommand2)
        assertEquals(doorCommand1.id, door1.id)
        assertEquals(doorCommand2.doorName, door1.doorName)
        assertEquals(doorCommand2.nfcCode, door1.nfcCode)
        assertEquals(doorCommand2.minLevelRole, door1.minLevelRole)
    }
}