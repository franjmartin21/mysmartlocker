package com.fmart43.mysmartlocker.controller

import com.fmart43.mysmartlocker.BaseDataMockTest
import com.fmart43.mysmartlocker.controller.command.EmployeeCommand
import com.fmart43.mysmartlocker.model.Role
import com.fmart43.mysmartlocker.service.EmployeeService
import org.hamcrest.Matchers
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.setup.MockMvcBuilders

internal class EmployeeControllerTest: BaseDataMockTest(){

    val employeeService = Mockito.mock(EmployeeService::class.java)

    var employeeController: EmployeeController? = null

    lateinit var mockMvc: MockMvc

    @BeforeEach
    fun setup(){
        employeeController = EmployeeController(employeeService)
        mockMvc = MockMvcBuilders.standaloneSetup(employeeController).build()
    }

    @Test
    fun initEmployee_success(){
        Mockito.`when`(employeeService.findEmployeeCommandById(1)).thenReturn(employeeCommand1)

        mockMvc.perform(get("/employee/1/edit"))
                .andExpect(status().isOk)
                .andExpect(view().name(employeeController!!.VIEW_EMPLOYEE))
                .andExpect(model().attributeExists(employeeController!!.EMPLOYEE_COMMAND))
                .andExpect(model().attribute(employeeController!!.EMPLOYEE_COMMAND, Matchers.equalTo(employeeCommand1)))

        Mockito.verify(employeeService, Mockito.times(1)).findEmployeeCommandById(1)
    }

    @Test
    fun saveOrUpdate_success() {
        var employeeCommand = EmployeeCommand(3, "elBotillas","abcdef", "firstName", "lastName", true, Role.ADMINISTRATOR)

        Mockito.`when`(employeeService.saveEmployeeCommand(employeeCommand)).thenReturn(employeeCommand)

        mockMvc.perform(post("/employee/3/save")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("username", "elBotillas")
                .param("password", "abcdef")
                .param("firstName", "firstName")
                .param("lastName", "lastName")
                .param("fingerprintEnabled", "true")
                .param("role", Role.ADMINISTRATOR.name))
                .andExpect(status().is3xxRedirection)

        Mockito.verify(employeeService, Mockito.times(1)).saveEmployeeCommand(employeeCommand)
    }

    @Test
    fun saveOrUpdate_validationError() {
        var employeeCommand = EmployeeCommand(3, username = "elBotillas")

        mockMvc.perform(post("/employee/3/save")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("username", "elBotillas"))
                .andExpect(status().is2xxSuccessful)
                .andExpect(view().name(employeeController!!.VIEW_EMPLOYEE))

        Mockito.verify(employeeService, Mockito.times(0)).saveEmployeeCommand(employeeCommand)
    }

}