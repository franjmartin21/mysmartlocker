package com.fmart43.mysmartlocker.repository

import com.fmart43.mysmartlocker.BaseDataMockTest
import com.fmart43.mysmartlocker.model.Employee
import com.fmart43.mysmartlocker.model.Role
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@DataJpaTest
@DirtiesContext
open class EmployeeRepositoryTest(@Autowired val employeeRepository: EmployeeRepository): BaseDataMockTest() {

    @BeforeEach
    fun setup(){
        employeeRepository.save(employee1)
        employeeRepository.save(employee2)
    }

    @Test
    fun findById_success() {
        val employee = employeeRepository.findById(1)
        Assertions.assertThat(employee?.username).isEqualTo(employee1.username)
        Assertions.assertThat(employee?.password).isEqualTo(employee1.password)
        Assertions.assertThat(employee?.firstName).isEqualTo(employee1.firstName)
        Assertions.assertThat(employee?.lastName).isEqualTo(employee1.lastName)
        Assertions.assertThat(employee?.fingerprintEnabled).isEqualTo(employee1.fingerprintEnabled)
        Assertions.assertThat(employee?.role).isEqualTo(employee1.role)
    }

    @Test
    fun findByUsername_success() {
        val employee = employeeRepository.findByUsername(employee1.username)
        Assertions.assertThat(employee?.username).isEqualTo(employee1.username)
        Assertions.assertThat(employee?.password).isEqualTo(employee1.password)
        Assertions.assertThat(employee?.firstName).isEqualTo(employee1.firstName)
        Assertions.assertThat(employee?.lastName).isEqualTo(employee1.lastName)
        Assertions.assertThat(employee?.fingerprintEnabled).isEqualTo(employee1.fingerprintEnabled)
        Assertions.assertThat(employee?.role).isEqualTo(employee1.role)
    }

    @Test
    fun findAll_success(){
        val employeeList: List<Employee>? = employeeRepository.findAll()
        Assertions.assertThat(employeeList).hasSize(2)
    }

    @Test
    fun save_success(){
        val employee3 = Employee("testingSave", "testing", "firstName", "lastName", true, Role.ADMINISTRATOR)
        Assertions.assertThat(employee3.id).isNull()
        Assertions.assertThat(employee3.createDate).isNull()
        Assertions.assertThat(employee3.updateDate).isNull()

        val employeeReturned = employeeRepository.save(employee3)

        Assertions.assertThat(employeeReturned?.id).isNotNull()
        Assertions.assertThat(employee3.createDate).isNotNull()
        Assertions.assertThat(employee3.updateDate).isNotNull()
    }
}