package com.fmart43.mysmartlocker.repository

import com.fmart43.mysmartlocker.BaseDataMockTest
import com.fmart43.mysmartlocker.model.*
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@DataJpaTest
@DirtiesContext
class AccessLogRepositoryTest(
        @Autowired val employeeRepository: EmployeeRepository,
        @Autowired val doorRepository: DoorRepository,
        @Autowired val accessLogRepository: AccessLogRepository): BaseDataMockTest() {

    @BeforeEach
    fun setup(){
        employeeRepository.save(employee1)
        employeeRepository.save(employee2)

        doorRepository.save(door1)
        doorRepository.save(door2)

        accessLogRepository.save(accessLog1)
        accessLogRepository.save(accessLog2)
        accessLogRepository.save(accessLog3)
        accessLogRepository.save(accessLog4)
    }

    @Test
    fun findAll_success(){
        val accessLogList: List<AccessLog> = accessLogRepository.findAll()
        Assertions.assertThat(accessLogList).hasSize(4)
        Assertions.assertThat(accessLogList.get(0).door).isEqualTo(door1)
        Assertions.assertThat(accessLogList.get(0).employee).isEqualTo(employee1)
        Assertions.assertThat(accessLogList.get(0).grantedAccess).isEqualTo(GrantedAccessEnum.GRANTED)
    }
}