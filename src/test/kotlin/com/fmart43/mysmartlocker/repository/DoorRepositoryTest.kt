package com.fmart43.mysmartlocker.repository

import com.fmart43.mysmartlocker.BaseDataMockTest
import com.fmart43.mysmartlocker.model.Door
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@DataJpaTest
@DirtiesContext
open class DoorRepositoryTest(@Autowired val doorRepository: DoorRepository): BaseDataMockTest(){

    var doorCreated1: Door? = null
    var doorCreated2: Door? = null

    @BeforeEach
    fun setup(){
        doorCreated1 = doorRepository.save(door1)
        doorCreated2 = doorRepository.save(door2)
    }

    @Test
    fun findById_success() {
        val door = doorRepository.findById(doorCreated1?.id!!)

        Assertions.assertThat(door?.doorName).isEqualTo(door1.doorName)
        Assertions.assertThat(door?.nfcCode).isEqualTo(door1.nfcCode)
        Assertions.assertThat(door?.minLevelRole).isEqualTo(door1.minLevelRole)
    }

    @Test
    fun findByNfcCode_success() {
        val door = doorRepository.findByNfcCode(doorCreated1?.nfcCode!!)

        Assertions.assertThat(door?.doorName).isEqualTo(door1.doorName)
        Assertions.assertThat(door?.nfcCode).isEqualTo(door1.nfcCode)
        Assertions.assertThat(door?.minLevelRole).isEqualTo(door1.minLevelRole)
    }

    @Test
    fun findAll_success(){
        val doorList: List<Door>? = doorRepository.findAll()
        Assertions.assertThat(doorList).hasSize(2)
    }

    @Test
    fun save_success(){
        val door4 = Door("door4", "door4", 2)
        Assertions.assertThat(door4.id).isNull()
        Assertions.assertThat(door4.createDate).isNull()
        Assertions.assertThat(door4.updateDate).isNull()

        val doorReturned = doorRepository.save(door4)

        Assertions.assertThat(doorReturned?.id).isNotNull()
        Assertions.assertThat(doorReturned?.createDate).isNotNull()
        Assertions.assertThat(doorReturned?.updateDate).isNotNull()
    }

    @Test
    fun delete_success(){
        val doorReturned2 = doorRepository.findById(doorCreated2?.id!!)
        Assertions.assertThat(doorReturned2).isNotNull()

        doorRepository.delete(doorReturned2!!)
        val doorReturned3 = doorRepository.findById(door2.id!!)
        Assertions.assertThat(doorReturned3).isNull()
    }
}