package com.fmart43.mysmartlocker.service

import com.fmart43.mysmartlocker.BaseDataMockTest
import com.fmart43.mysmartlocker.exception.NotFoundException
import com.fmart43.mysmartlocker.model.Employee
import com.fmart43.mysmartlocker.model.Role
import com.fmart43.mysmartlocker.repository.EmployeeRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito


internal class EmployeeServiceTest: BaseDataMockTest(){

    val employeeRepository = Mockito.mock(EmployeeRepository::class.java)

    lateinit var employeeService: EmployeeService

    lateinit var employeeListReturned: List<Employee>

    @BeforeEach
    fun setup(){
        employeeService = EmployeeService(employeeRepository)

        employeeListReturned = listOf(employee1, employee2)
    }

    @Test
    fun getListEmployees() {
        Mockito.`when`(employeeRepository.findAll()).thenReturn(employeeListReturned)

        val employeeList: List<Employee>? = employeeService.getListEmployees()

        Assertions.assertEquals(2, employeeList?.size)
        Mockito.verify(employeeRepository, Mockito.times(1)).findAll()
    }

    @Test
    fun getEmployeeRepositoryByRole_success() {
        Mockito.`when`(employeeRepository.findAll()).thenReturn(employeeListReturned)

        val employeeList: List<Employee>? = employeeService.getListEmployees(Role.ADVANCED)

        Assertions.assertEquals(1, employeeList?.size)
        Assertions.assertEquals(employee2.username, employeeList?.get(0)?.username)

        Mockito.verify(employeeRepository, Mockito.times(1)).findAll()
    }

    @Test
    fun findEmployeeCommandById_success(){
        val idToFind = 1
        employee1.id = idToFind
        Mockito.`when`(employeeRepository.findById(idToFind)).thenReturn(employee1)

        val employeeCommand = employeeService.findEmployeeCommandById(idToFind)

        Assertions.assertEquals(1, employeeCommand?.id)
        Assertions.assertEquals(employee1.username, employeeCommand?.username)
        Assertions.assertEquals(employee1.password, employeeCommand?.password)
        Assertions.assertEquals(employee1.firstName, employeeCommand?.firstName)
        Assertions.assertEquals(employee1.lastName, employeeCommand?.lastName)
        Assertions.assertEquals(employee1.fingerprintEnabled, employeeCommand?.fingerprintEnabled)
        Assertions.assertEquals(employee1.role, employeeCommand?.role)

        Mockito.verify(employeeRepository, Mockito.times(1)).findById(idToFind)
    }

    @Test
    fun findEmployeeCommandById_exception(){
        val idToFind = 6
        Mockito.`when`(employeeRepository.findById(idToFind)).thenReturn(null)

        val exception = Assertions.assertThrows(NotFoundException::class.java) {
            employeeService.findEmployeeCommandById(idToFind)
        }
        Assertions.assertEquals(NotFoundException.EMPLOYEE(idToFind).message, exception.message)
        Mockito.verify(employeeRepository, Mockito.times(1)).findById(idToFind)
    }


    @Test
    fun saveEmployeeCommand_success(){
        employee1.id = 1
        Mockito.`when`(employeeRepository.save(employee1)).thenReturn(employee1)

        val employeeCommand = employeeService.saveEmployeeCommand(employeeCommand1)

        Assertions.assertEquals(1, employeeCommand?.id)
        Assertions.assertEquals(employee1.username, employeeCommand?.username)
        Assertions.assertEquals(employee1.password, employeeCommand?.password)
        Assertions.assertEquals(employee1.firstName, employeeCommand?.firstName)
        Assertions.assertEquals(employee1.lastName, employeeCommand?.lastName)
        Assertions.assertEquals(employee1.fingerprintEnabled, employeeCommand?.fingerprintEnabled)
        Assertions.assertEquals(employee1.role, employeeCommand?.role)

        Mockito.verify(employeeRepository, Mockito.times(1)).save(employee1)
    }

    @Test
    fun saveEmployeeDTO_success(){
        employee1.role = Role.UNAUTHORIZED
        employee1.id = 1
        Mockito.`when`(employeeRepository.save(employee1)).thenReturn(employee1)

        val employeeDTO = employeeService.saveEmployeeDTO(employeeDTO1)

        Assertions.assertEquals(1, employeeDTO?.id)
        Assertions.assertEquals(employee1.username, employeeDTO?.username)
        Assertions.assertEquals(employee1.password, employeeDTO?.password)
        Assertions.assertEquals(employee1.firstName, employeeDTO?.firstName)
        Assertions.assertEquals(employee1.lastName, employeeDTO?.lastName)
        Assertions.assertEquals(employee1.fingerprintEnabled, employeeDTO?.fingerprintEnabled)

        Mockito.verify(employeeRepository, Mockito.times(1)).save(employee1)
    }
}