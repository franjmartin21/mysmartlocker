package com.fmart43.mysmartlocker.service

import com.fmart43.mysmartlocker.BaseDataMockTest
import com.fmart43.mysmartlocker.exception.NotFoundException
import com.fmart43.mysmartlocker.model.Door
import com.fmart43.mysmartlocker.repository.DoorRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito

internal class DoorServiceTest: BaseDataMockTest(){

    val doorRepository = Mockito.mock(DoorRepository::class.java)

    lateinit var doorService: DoorService

    lateinit var doorListReturned: List<Door>


    @BeforeEach
    fun setup(){
        doorService = DoorService(doorRepository)
        doorListReturned = listOf(door1, door2, door3)
    }

    @Test
    fun getListEmployees() {
        Mockito.`when`(doorRepository.findAll()).thenReturn(doorListReturned)

        val doorList: List<Door>? = doorService.getListDoors()

        Assertions.assertEquals(3, doorList?.size)
        Mockito.verify(doorRepository, Mockito.times(1)).findAll()
    }

    @Test
    fun findDoorCommandById_success(){
        val idToFind = 1
        door1.id = idToFind
        Mockito.`when`(doorRepository.findById(idToFind)).thenReturn(door1)

        val doorCommand = doorService.findDoorCommandById(idToFind)

        Assertions.assertEquals(1, doorCommand?.id)
        Assertions.assertEquals(door1.doorName, doorCommand?.doorName)
        Assertions.assertEquals(door1.nfcCode, doorCommand?.nfcCode)
        Assertions.assertEquals(door1.minLevelRole, doorCommand?.minLevelRole)

        Mockito.verify(doorRepository, Mockito.times(1)).findById(idToFind)
    }

    @Test
    fun findDoorCommandById_exception(){
        val idToFind = 10
        door1.id = idToFind
        Mockito.`when`(doorRepository.findById(idToFind)).thenReturn(null)

        val exception = Assertions.assertThrows(NotFoundException::class.java) {
            doorService.findDoorCommandById(idToFind)
        }

        Assertions.assertEquals(NotFoundException.DOOR(idToFind).message, exception.message)
        Mockito.verify(doorRepository, Mockito.times(1)).findById(idToFind)
    }

    @Test
    fun saveDoorCommand_newdoor_success(){
        door1.id = 1
        doorCommand1.id = null
        Mockito.`when`(doorRepository.save(door1)).thenReturn(door1)

        val doorReturned = doorService.saveDoorCommand(doorCommand1)

        Assertions.assertEquals(1, doorReturned?.id)
        Assertions.assertEquals(door1.doorName, doorReturned?.doorName)
        Assertions.assertEquals(door1.nfcCode, doorReturned?.nfcCode)
        Assertions.assertEquals(door1.minLevelRole, doorReturned?.minLevelRole)

        Mockito.verify(doorRepository, Mockito.times(0)).findById(1)
        Mockito.verify(doorRepository, Mockito.times(1)).save(door1)
    }

    @Test
    fun saveDoorCommand_editdoor_success(){
        door1.id = 1
        door2.id = 1
        doorCommand2.id = 1
        Mockito.`when`(doorRepository.findById(1)).thenReturn(door1)
        Mockito.`when`(doorRepository.save(door2)).thenReturn(door2)

        val doorReturned = doorService.saveDoorCommand(doorCommand2)

        Assertions.assertEquals(1, doorReturned?.id)
        Assertions.assertEquals(door2.doorName, doorReturned?.doorName)
        Assertions.assertEquals(door2.nfcCode, doorReturned?.nfcCode)
        Assertions.assertEquals(door2.minLevelRole, doorReturned?.minLevelRole)

        Mockito.verify(doorRepository, Mockito.times(1)).findById(1)
        Mockito.verify(doorRepository, Mockito.times(1)).save(door2)
    }


    @Test
    fun deleteDoorCommand_success(){
        doorService.deleteDoorCommand(doorCommand1)
        Mockito.verify(doorRepository, Mockito.times(1)).delete(door1)
    }
}