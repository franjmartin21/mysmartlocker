package com.fmart43.mysmartlocker.service

import com.fmart43.mysmartlocker.BaseDataMockTest
import com.fmart43.mysmartlocker.model.*
import com.fmart43.mysmartlocker.repository.AccessLogRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito

internal class AccessLogServiceTest: BaseDataMockTest() {

    val accessLogRepository = Mockito.mock(AccessLogRepository::class.java)

    lateinit var accessLogService: AccessLogService

    lateinit var accessLogListReturned: List<AccessLog>

    @BeforeEach
    fun setup(){
        accessLogService = AccessLogService(accessLogRepository)
        employee1.id = 1
        employee2.id = 2

        door1.id = 1
        door2.id = 2
        door3.id = 3

        accessLogListReturned = listOf(
                accessLog1,
                accessLog2,
                accessLog3,
                accessLog4
        )
    }

    @Test
    fun getListAccessLog_success() {
        Mockito.`when`(accessLogRepository.findAll()).thenReturn(accessLogListReturned)

        val accessLogList: List<AccessLog> = accessLogService.getAccessLog()

        Assertions.assertEquals(4, accessLogList.size)
        Mockito.verify(accessLogRepository, Mockito.times(1)).findAll()
    }

    @Test
    fun getListAccessLogByEmployee_success() {
        Mockito.`when`(accessLogRepository.findAll()).thenReturn(accessLogListReturned)

        val accessLogList: List<AccessLog> = accessLogService.getAccessLog(employeeId = employee1.id)

        Assertions.assertEquals(2, accessLogList.size)
        Assertions.assertEquals(employee1, accessLogList.get(0).employee)
        Assertions.assertEquals(employee1, accessLogList.get(1).employee)
        Mockito.verify(accessLogRepository, Mockito.times(1)).findAll()
    }

    @Test
    fun getListAccessLogByDoor_success() {
        Mockito.`when`(accessLogRepository.findAll()).thenReturn(accessLogListReturned)

        val accessLogList: List<AccessLog> = accessLogService.getAccessLog(doorId = door2.id)

        Assertions.assertEquals(2, accessLogList.size)
        Assertions.assertEquals(door2, accessLogList.get(0).door)
        Assertions.assertEquals(door2, accessLogList.get(1).door)
        Mockito.verify(accessLogRepository, Mockito.times(1)).findAll()
    }

    @Test
    fun getListAccessLogByGrantedAccess_success() {
        Mockito.`when`(accessLogRepository.findAll()).thenReturn(accessLogListReturned)

        val accessLogList: List<AccessLog> = accessLogService.getAccessLog(grantedAccess = GrantedAccessEnum.GRANTED)

        Assertions.assertEquals(2, accessLogList.size)
        Assertions.assertEquals(GrantedAccessEnum.GRANTED, accessLogList.get(0).grantedAccess)
        Assertions.assertEquals(GrantedAccessEnum.GRANTED, accessLogList.get(1).grantedAccess)
        Mockito.verify(accessLogRepository, Mockito.times(1)).findAll()
    }
}