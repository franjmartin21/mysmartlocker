package com.fmart43.mysmartlocker.service

import com.fmart43.mysmartlocker.BaseDataMockTest
import com.fmart43.mysmartlocker.exception.AuthenticationFailedServiceException
import com.fmart43.mysmartlocker.exception.NotFoundException
import com.fmart43.mysmartlocker.exception.UnauthorizedServiceException
import com.fmart43.mysmartlocker.repository.AccessLogRepository
import com.fmart43.mysmartlocker.repository.DoorRepository
import com.fmart43.mysmartlocker.repository.EmployeeRepository
import com.fmart43.mysmartlocker.restcontroller.v1.dto.CheckEmployeeDoorResponseDTO
import com.fmart43.mysmartlocker.restcontroller.v1.dto.EmployeeDoorDTO
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito

internal class AuthorizationServiceTest: BaseDataMockTest(){

    val employeeRepository = Mockito.mock(EmployeeRepository::class.java)
    val doorRepository = Mockito.mock(DoorRepository::class.java)
    val accessLogRepository = Mockito.mock(AccessLogRepository::class.java)

    lateinit var authorizationService: AuthorizationService

    @BeforeEach
    fun setup(){
        authorizationService = AuthorizationService(employeeRepository, doorRepository, accessLogRepository)
    }


    @Test
    fun isEmployeeAuthorizedForDoor_success() {
        Mockito.`when`(employeeRepository.findByUsername(employee1.username)).thenReturn(employee1)
        Mockito.`when`(doorRepository.findByNfcCode(door1.nfcCode)).thenReturn(door1)

        val employeeDoorDTO = EmployeeDoorDTO(door1.nfcCode, employee1.username)

        val result = authorizationService.isEmployeeAuthorizedForDoor(employeeDoorDTO)

        Assertions.assertEquals(door1.doorName, result?.doorName)
        Assertions.assertEquals(employee1.username, result?.userName)

        Mockito.verify(employeeRepository, Mockito.times(1)).findByUsername(employee1.username)
        Mockito.verify(doorRepository, Mockito.times(1)).findByNfcCode(door1.nfcCode)
    }

    @Test
    fun isEmployeeAuthorizedForDoor_denied() {
        Mockito.`when`(employeeRepository.findByUsername(employee1.username)).thenReturn(employee1)
        Mockito.`when`(doorRepository.findByNfcCode(door2.nfcCode)).thenReturn(door2)

        val employeeDoorDTO = EmployeeDoorDTO(door2.nfcCode, employee1.username)

        val result = authorizationService.isEmployeeAuthorizedForDoor(employeeDoorDTO)

        Assertions.assertNull(result)

        Mockito.verify(employeeRepository, Mockito.times(1)).findByUsername(employee1.username)
        Mockito.verify(doorRepository, Mockito.times(1)).findByNfcCode(door2.nfcCode)
        Mockito.verify(accessLogRepository, Mockito.times(1)).save(accessLog3)
    }

    @Test
    fun isEmployeeAuthorizedForDoor_employeeNotFound() {
        val NOT_STORED_EMPLOYEE = "notstoreduser@notstored.com"

        Mockito.`when`(employeeRepository.findByUsername(NOT_STORED_EMPLOYEE)).thenReturn(null)
        val employeeDoorDTO = EmployeeDoorDTO(door2.nfcCode, NOT_STORED_EMPLOYEE)

        val exception = Assertions.assertThrows(NotFoundException::class.java) {
            authorizationService.isEmployeeAuthorizedForDoor(employeeDoorDTO)
        }

        Assertions.assertEquals(NotFoundException.EMPLOYEE(NOT_STORED_EMPLOYEE).message, exception.message)
        Mockito.verify(employeeRepository, Mockito.times(1)).findByUsername(NOT_STORED_EMPLOYEE)
    }

    @Test
    fun isEmployeeAuthorizedForDoor_doorNotFound() {
        val NOT_STORED_DOOR = "NOTFOUND"

        Mockito.`when`(employeeRepository.findByUsername(employee1.username)).thenReturn(employee1)
        Mockito.`when`(doorRepository.findByNfcCode(NOT_STORED_DOOR)).thenReturn(null)
        val employeeDoorDTO = EmployeeDoorDTO(NOT_STORED_DOOR, employee1.username)

        val exception = Assertions.assertThrows(NotFoundException::class.java) {
            authorizationService.isEmployeeAuthorizedForDoor(employeeDoorDTO)
        }

        Assertions.assertEquals(NotFoundException.DOOR(NOT_STORED_DOOR).message, exception.message)
        Mockito.verify(employeeRepository, Mockito.times(1)).findByUsername(employee1.username)
        Mockito.verify(doorRepository, Mockito.times(1)).findByNfcCode(NOT_STORED_DOOR)
    }



    @Test
    fun authorizeEmployeeForDoor_success() {
        Mockito.`when`(employeeRepository.findByUsername(employee1.username)).thenReturn(employee1)
        Mockito.`when`(doorRepository.findByNfcCode(door1.nfcCode)).thenReturn(door1)

        val employeeDoorDTO = EmployeeDoorDTO(door1.nfcCode, employee1.username, employee1.password)

        authorizationService.authorizeEmployeeForDoor(employeeDoorDTO)

        Mockito.verify(employeeRepository, Mockito.times(1)).findByUsername(employee1.username)
        Mockito.verify(doorRepository, Mockito.times(1)).findByNfcCode(door1.nfcCode)
        Mockito.verify(accessLogRepository, Mockito.times(1)).save(accessLog1)
    }

    @Test
    fun authorizeEmployeeForDoor_denied() {
        Mockito.`when`(employeeRepository.findByUsername(employee1.username)).thenReturn(employee1)
        Mockito.`when`(doorRepository.findByNfcCode(door2.nfcCode)).thenReturn(door2)

        val employeeDoorDTO = EmployeeDoorDTO(door2.nfcCode, employee1.username, employee1.password)

        val exception = Assertions.assertThrows(UnauthorizedServiceException::class.java) {
            authorizationService.authorizeEmployeeForDoor(employeeDoorDTO)
        }

        Mockito.verify(employeeRepository, Mockito.times(1)).findByUsername(employee1.username)
        Mockito.verify(doorRepository, Mockito.times(1)).findByNfcCode(door2.nfcCode)
        Mockito.verify(accessLogRepository, Mockito.times(1)).save(accessLog3)
    }

    @Test
    fun authorizeEmployeeForDoor_authenticationFailed() {
        val WRONG_PASSWORD = "any_wrong_password"

        Mockito.`when`(employeeRepository.findByUsername(employee1.username)).thenReturn(employee1)

        val employeeDoorDTO = EmployeeDoorDTO(door2.nfcCode, employee1.username, WRONG_PASSWORD)

        val exception = Assertions.assertThrows(AuthenticationFailedServiceException::class.java) {
            authorizationService.authorizeEmployeeForDoor(employeeDoorDTO)
        }

        Mockito.verify(employeeRepository, Mockito.times(1)).findByUsername(employee1.username)
    }
}