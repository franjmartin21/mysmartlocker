package com.fmart43.mysmartlocker

import com.fmart43.mysmartlocker.controller.command.DoorCommand
import com.fmart43.mysmartlocker.controller.command.EmployeeCommand
import com.fmart43.mysmartlocker.model.*
import com.fmart43.mysmartlocker.restcontroller.v1.dto.EmployeeDTO

open class BaseDataMockTest{
    val employee1 = Employee("testuser", "password", "firstName", "lastName", true, Role.BASIC)
    val employee2 = Employee("jjjmmm", "adsf", "Francisco", "Martin", true, Role.ADVANCED)

    val door1 = Door("Basic Door", "DOOR-1", Role.BASIC.levelAccess)
    val door2 = Door("Advanced Door", "DOOR-2", Role.ADVANCED.levelAccess)
    val door3 = Door("Administrator Door", "DOOR-3", Role.ADMINISTRATOR.levelAccess)

    val accessLog1 = AccessLog(employee1, door1, GrantedAccessEnum.GRANTED)
    val accessLog2 = AccessLog(employee2, door1, GrantedAccessEnum.DENIED)
    val accessLog3 = AccessLog(employee1, door2, GrantedAccessEnum.DENIED)
    val accessLog4 = AccessLog(employee2, door2, GrantedAccessEnum.GRANTED)
    val accessLog5 = AccessLog(employee1, door3, GrantedAccessEnum.DENIED)
    val accessLog6 = AccessLog(employee2, door3, GrantedAccessEnum.GRANTED)

    val employeeCommand1 = EmployeeCommand(1, "testuser", "password", "firstName", "lastName", true, Role.BASIC)
    val employeeCommand2 = EmployeeCommand(2, "jjjmmm", "adsf", "Francisco", "Martin", true, Role.ADVANCED)

    val employeeDTO1 = EmployeeDTO(1, "testuser", "password", "firstName", "lastName", true)
    val employeeDTO2 = EmployeeDTO(2, "jjjmmm", "adsf", "Francisco", "Martin", true)

    val doorCommand1 = DoorCommand(1, "Basic Door", "DOOR-1", Role.BASIC.levelAccess)
    val doorCommand2 = DoorCommand(2, "Advanced Door", "DOOR-2", Role.ADVANCED.levelAccess)
    val doorCommand3 = DoorCommand(3, "Administrator Door", "DOOR-3", Role.ADMINISTRATOR.levelAccess)
}