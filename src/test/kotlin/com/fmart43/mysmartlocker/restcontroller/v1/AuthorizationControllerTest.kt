package com.fmart43.mysmartlocker.restcontroller.v1

import com.fasterxml.jackson.databind.ObjectMapper
import com.fmart43.mysmartlocker.BaseDataMockTest
import com.fmart43.mysmartlocker.exception.AuthenticationFailedServiceException
import com.fmart43.mysmartlocker.exception.NotFoundException
import com.fmart43.mysmartlocker.exception.UnauthorizedServiceException
import com.fmart43.mysmartlocker.restcontroller.RestResponseEntityExceptionHandler
import com.fmart43.mysmartlocker.restcontroller.v1.dto.CheckEmployeeDoorResponseDTO
import com.fmart43.mysmartlocker.restcontroller.v1.dto.EmployeeDoorDTO
import com.fmart43.mysmartlocker.service.AuthorizationService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders

internal class AuthorizationControllerTest: BaseDataMockTest(){

    val authorizationService = Mockito.mock(AuthorizationService::class.java)

    var authorizationController: AuthorizationController? = null

    lateinit var mockMvc: MockMvc

    @BeforeEach
    fun setup(){
        authorizationController = AuthorizationController(authorizationService)
        mockMvc = MockMvcBuilders.standaloneSetup(authorizationController)
                .setControllerAdvice(RestResponseEntityExceptionHandler()).build()
    }

    @Test
    fun checkAuthorization_success(){
        val employeeDoorDTO = EmployeeDoorDTO(door1.nfcCode, employee1.username)
        val checkEmployeeDoorResponseDTO = CheckEmployeeDoorResponseDTO("abc", door1.doorName, employee1.username)

        Mockito.`when`(authorizationService.isEmployeeAuthorizedForDoor(employeeDoorDTO)).thenReturn(checkEmployeeDoorResponseDTO)

        mockMvc.perform(post("/api/v1/authorization/check")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapper().writeValueAsString(employeeDoorDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())

        Mockito.verify(authorizationService, Mockito.times(1)).isEmployeeAuthorizedForDoor(employeeDoorDTO)
    }

    @Test
    fun checkAuthorization_unauthorized(){
        val employeeDoorDTO = EmployeeDoorDTO(door1.nfcCode, employee1.username)
        val checkEmployeeDoorResponseDTO = null

        Mockito.`when`(authorizationService.isEmployeeAuthorizedForDoor(employeeDoorDTO)).thenReturn(checkEmployeeDoorResponseDTO)

        mockMvc.perform(post("/api/v1/authorization/check")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapper().writeValueAsString(employeeDoorDTO)))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized)

        Mockito.verify(authorizationService, Mockito.times(1)).isEmployeeAuthorizedForDoor(employeeDoorDTO)
    }

    @Test
    fun checkAuthorization_returnedNotFoundException(){
        val employeeDoorDTO = EmployeeDoorDTO(door1.nfcCode, employee1.username)

        Mockito.doAnswer { throw NotFoundException.EMPLOYEE(employee1.username) }.`when`(authorizationService).isEmployeeAuthorizedForDoor(employeeDoorDTO)

        mockMvc.perform(post("/api/v1/authorization/check")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapper().writeValueAsString(employeeDoorDTO)))
                .andExpect(MockMvcResultMatchers.status().isExpectationFailed)

        Mockito.verify(authorizationService, Mockito.times(1)).isEmployeeAuthorizedForDoor(employeeDoorDTO)
    }


    @Test
    fun authorize_success(){
        val employeeDoorDTO = EmployeeDoorDTO(door1.nfcCode, employee1.username, employee1.password)

        mockMvc.perform(post("/api/v1/authorization/authorize")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapper().writeValueAsString(employeeDoorDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk)

        Mockito.verify(authorizationService, Mockito.times(1)).authorizeEmployeeForDoor(employeeDoorDTO)
    }

    @Test
    fun authorize_unauthorized(){
        val employeeDoorDTO = EmployeeDoorDTO(door1.nfcCode, employee1.username, employee1.password)

        Mockito.doAnswer{ throw UnauthorizedServiceException() }.`when`(authorizationService).authorizeEmployeeForDoor(employeeDoorDTO)

        mockMvc.perform(post("/api/v1/authorization/authorize")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapper().writeValueAsString(employeeDoorDTO)))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized)

        Mockito.verify(authorizationService, Mockito.times(1)).authorizeEmployeeForDoor(employeeDoorDTO)
    }

    @Test
    fun authorize_authenticationFailed(){
        val employeeDoorDTO = EmployeeDoorDTO(door1.nfcCode, employee1.username, employee1.password)

        Mockito.doAnswer{ throw AuthenticationFailedServiceException() }.`when`(authorizationService).authorizeEmployeeForDoor(employeeDoorDTO)

        mockMvc.perform(post("/api/v1/authorization/authorize")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapper().writeValueAsString(employeeDoorDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest)

        Mockito.verify(authorizationService, Mockito.times(1)).authorizeEmployeeForDoor(employeeDoorDTO)
    }
}