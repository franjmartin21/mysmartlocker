package com.fmart43.mysmartlocker.restcontroller.v1

import com.fasterxml.jackson.databind.ObjectMapper
import com.fmart43.mysmartlocker.BaseDataMockTest
import com.fmart43.mysmartlocker.MysmartlockerApplication
import com.fmart43.mysmartlocker.conf.WebSecurityConfiguration
import com.fmart43.mysmartlocker.restcontroller.v1.dto.EmployeeDTO
import com.fmart43.mysmartlocker.service.EmployeeService
import com.fmart43.mysmartlocker.service.ImplementationSettingService
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@ExtendWith(SpringExtension::class)
@WebMvcTest(RegistrationController::class, ImplementationSettingService::class)
@ContextConfiguration(classes = [MysmartlockerApplication::class, WebSecurityConfiguration::class])
internal class RegistrationControllerTest: BaseDataMockTest(){

    @Value("\${conf.nfcCode}")
    val nfcCode: String? = null

    @Value("\${conf.companyName}")
    val companyName: String? = null

    @Autowired
    lateinit var mockMvc: MockMvc

    @MockBean
    lateinit var employeeService: EmployeeService

    @Test
    fun startRegistration_success(){
        mockMvc.perform(get("/api/v1/registration/start")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath<String>("$.nameCompany", equalTo<String>(companyName)))
                .andExpect(jsonPath<String>("$.nfcCode", equalTo<String>(nfcCode)))
    }

    @Test
    fun completeRegistration_success(){
        val employeeDTO = EmployeeDTO()
        employeeDTO.username = "elbotillas@boti.com"
        employeeDTO.password = "asdfasdf"
        employeeDTO.firstName = "Botillas"
        employeeDTO.lastName = "elFani"

        Mockito.`when`(employeeService.saveEmployeeDTO(employeeDTO)).thenReturn(employeeDTO)

        mockMvc.perform(post("/api/v1/registration/complete")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapper().writeValueAsString(employeeDTO)))
                .andExpect(status().isCreated())

        Mockito.verify(employeeService, Mockito.times(1)).saveEmployeeDTO(employeeDTO)
    }

    @Test
    fun completeRegistration_validationError(){
        val employeeDTO = EmployeeDTO()
        employeeDTO.username = "elbotillas@boti.com"
        employeeDTO.password = "asdfasdf"
        employeeDTO.firstName = "Botillas"
        employeeDTO.lastName = ""

        mockMvc.perform(post("/api/v1/registration/complete")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapper().writeValueAsString(employeeDTO)))
                .andExpect(status().isBadRequest)

        Mockito.verify(employeeService, Mockito.times(0)).saveEmployeeDTO(employeeDTO)
    }

    @Test
    fun completeRegistration_serverError(){
        val employeeDTO = EmployeeDTO()
        employeeDTO.username = "elbotillas@boti.com"
        employeeDTO.password = "asdfasdf"
        employeeDTO.firstName = "Botillas"
        employeeDTO.lastName = "elFani"

        Mockito.`when`(employeeService.saveEmployeeDTO(employeeDTO)).thenReturn(null)

        mockMvc.perform(post("/api/v1/registration/complete")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapper().writeValueAsString(employeeDTO)))
                .andExpect(status().is5xxServerError)

        Mockito.verify(employeeService, Mockito.times(1)).saveEmployeeDTO(employeeDTO)
    }
}