package com.fmart43.mysmartlocker.model

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class RoleTest{

    @Test
    fun findRolesWithAnyAuthorization_success(){
        val roles = Role.findRolesWithAnyAuthorization()
        Assertions.assertTrue(!roles.contains(Role.UNAUTHORIZED))
        Assertions.assertTrue(roles.contains(Role.BASIC))
        Assertions.assertTrue(roles.contains(Role.ADVANCED))
        Assertions.assertTrue(roles.contains(Role.ADMINISTRATOR))
    }
}