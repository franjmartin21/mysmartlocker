-- MY SMART LOCKER --

This project was created for the WGU Capstone project by Francisco J. Martin de Blas

My Smart Locker is the the second of two deliverables that are part of the whole solution for a new Electronic Access Control (EAC) system
This is the server side application that allows the administration of the infrastructure and comunication with the Android application

-- Instalation of the application

Prerrequisites:

You will need to have Java 1.8.0_181 or above

STEPS:

1- Download the last artifact generated in the last commit by clicking in the CI/CD button and clicking in the "Download Build Artifacts" drop down which is at the right side of each build.

2- unzip the file and find the .jar file that should be under build/libs

3. run: java -jar <jar file>


-- Alternative installation

Prerequisites:

You will need to have Docker and Docker compose installed in your machine

STEPS:

1- Clone the repository in your machine

2- Being in the root of the project cloned, Run: docker-compose up

It could take some time to run depending on your machine and network as it will need to setup the docker image and run gradle to download dependencies 

-- Loging into the admin console

1- Once you have started up the project you can access the admin console by using the url: http://localhost:8080

2- You can access the console with credentials. user: "user", password: "password"